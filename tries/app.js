`use strict`;

const array = [
  {
    name: "Valya",
    age: 12,
  },
  {
    name: "Olya",
    age: 34,
  },
  {
    name: "Gosha",
    age: 76,
  },
];

const root = document.querySelector("#root");
function fil(age) {
  const arrayFilter = array.filter((ar) => {
    return ar.age < age;
  });
  console.log(arrayFilter);
}
fil(30);
