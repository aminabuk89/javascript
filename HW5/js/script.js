"use strict";

/* 
                    Теоретичні питання:

    1. Як можна сторити функцію та як ми можемо її викликати? 

Створити функцію можна двома основними способами:
1. function declaration (оголошення функції) - ця функція підіймається вгору, це значить, що функція доступна в усій області видимості. Її викликати можна ще перед оголошенням фунції, за допомогою аргументів.
function nameFunction (parameteres){
    тіло функції;
    return value;
}
2. function expression - бере функцію та присваює їй змінну. Ця функція на підіймається вгору, а це значить, що вона не доступна в усій області видимості. Тобто, її викликати можна лише після оголошення. Ця функція може бути анонімна (використовується лише тоді, коли фунція більше не буде викликаться) або іменована:
1. анонімна:
const nameVariable = function (parameteres){
    тіло функції;
    return value;
}
2. іменована:
const nameVariable = function nameFunction (parameteres){
    тіло функції;
    return value;
}

    2. Що таке оператор return в JavaScript? Як його використовувати в функціях?

Оператор повернення (return) - використовується для завершення функції, та повернення її значення. Наприклад:
function summValue (a, b){
return a + b;                           //повернення а + b
}
let result = summValue (65 + 43);       //викор. повернених значень у функцію
console.log(result)

    3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?

Параметри функцій - параметри, які використовуються в () дужках під час оголошення функції. Вони представляють значення, які функція буде використовувати під час оголошення.
Аргументи фунції - фактичні значення, які передаються функції під час виклику, використовуються в () дужках. Вони замінюють параметри всередині функцій, вказуються у відповідності з параметрами. Функції можуть бути викликані з будь-якою кількістю аргументів, незалежно від параметрів. Якщо є додаткові аргументи, то - ігнор; якщо відсутні аргументи, то значення параметрів undefined.
Інода параметри і аргументи можуть бути взаємозамінними, але все ж таки вони різні.

    4. Як передати функцію аргументом в іншу функцію? 

Для того, щоб передати функцію аргументом в іншу функцію, то спочатку створюємо fn1 (аргумент) і в середині цієї функції викликаємо аргумент(), далі створюємо fn2(). І за функціями робимо виклик функції з передачею іншої функції як аргументу: fn1(fn2).


                    Практичні завдання:

    1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
 
function divisionVariables(a, b) {
  return a / b;
}
let result = divisionVariables(89, 7);
console.log(result);

    2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.


*/

// let number1, number2, operationSelection;
// while (true) {
//   number1 = prompt("Введіть, будь ласка, перше число");
//   if (number1 !== null && number1 !== "" && !isNaN(number1)) {
//     number1 = parseFloat(number1);
//     console.log(number1);
//     break;
//   } else {
//     alert("Введіть, будь ласка, саме число!");
//   }
// }
// while (true) {
//   number2 = prompt("Введіть, будь ласка, друге число");
//   if (number2 !== null && number2 !== "" && !isNaN(number2)) {
//     number2 = parseFloat(number2);
//     console.log(number2);
//     break;
//   } else {
//     alert("Введіть, будь ласка, саме число!");
//   }
// }

// operationSelection = prompt(
//   "Введіть, будь ласка, математичну операцію, яку необхідно здійснити над введеними числами."
// );
// function passValues(number1, number2, operationSelection) {
//   if (
//     operationSelection === `+` ||
//     operationSelection === `-` ||
//     operationSelection === `/` ||
//     operationSelection === `*`
//   ) {
//     let result;
//     switch (operationSelection) {
//       case `+`:
//         result = number1 + number2;
//         break;
//       case `-`:
//         result = number1 - number2;
//         break;
//       case `/`:
//         if (number2 !== 0) {
//           return number1 / number2;
//         } else {
//           alert("На 0 не ділиться!");
//         }
//         break;
//       case `*`:
//         result = number1 * number2;
//         break;
//     }
//     return result;
//   } else {
//     alert("Такої операції не існує!");
//   }
// }

// console.log(passValues(number1, number2, operationSelection));

//    3. Опціонально. Завдання:
// Реалізувати функцію підрахунку факторіалу числа.
// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера число, яке введе користувач.
// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.
