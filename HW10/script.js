"use strict";
/* 
                    Теоретичні питання:

    1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
    
    Створення нових елементів:
1. .createElement("тег") - цей метод створює новий елемент.
2. властивість innerHTML - в рядку створює новий елемент і текст в середині нього.

    Додавання нових DOM-елементів на сторінку:
1. prepend()
2. append()
3. before()
4. after()

    2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

Якщо ми хочемо видалити елемент зі сторінки, то можемо скористатися методом .remove() - видалиться повністю цей едемент. Наприклад:
1. const el = document.querySelector(".feature-title");          //дісталися до першого елемента за селектором .feature-title
el.remove();                                                     //видалили пергий тег h2 на сторінці сторінці

2. const el = document.querySelector(".feature-title");          //дісталися до першого елемента за селектором .feature-title
el.classList.add("navigation");                                  //через властивість classList і її метод .add("") додаємо клас navigation до нащого h2                                                               клас navigation  до нашого h2
el.style.color = "red";                                          //колір тексту в класі navigation змінили на червоний
el.classList.remove("navigation");                               //видалили клас navigation
el.style.color = "";                                             //скинули стилі (за замовченням колір чорний в тексті)

    3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

1. prepend() - вставити першим елементом після батька (в середині)
2. append() - вставити останнім елементом у батька (в середині)
3. before() - вставити  елемент перед батьком (за межами)
4. after() - вставити елемент після батька (за межами)


                    Практичні завдання:

    1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
const link = document.createElement("a");
link.textContent = "Learn More";
link.href = "#";
const footer = document.querySelector("footer");
const lastParagraf = footer.lastElementChild;
lastParagraf.after(link);

    2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

// const selectEl = document.createElement("select");
// selectEl.id = "rating";
// const section = document.querySelector(".features");
// for (let i = 1; i < 5; i++) {
//   const optionEl = document.createElement("option");
//   optionEl.value = `${i}`;
//   optionEl.textContent = `${i} Stars`;
//   selectEl.prepend(optionEl);
// }
// section.before(selectEl);
