"use strict";

/* 
                    Теоретичні питання:

    1. Опишіть своїми словами як працює метод forEach.

Метод forEach - перебирає кожен елемент масиву (передає в середину колбеку значення, індекс та масив з якого взято значення). Не обов'язково використовувати всі три параменти. Без колбеку метод не працює.

    2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади. 

let arr = ["hello, peace", 769, null, function () {}, "1234567"];
let arrNew;

    Мутують існуючий масив:

1. push()
console.log(arr.push("end"));
console.log(arr);
2. pop()
console.log(arr.pop());
console.log(arr);
3. unshift()
console.log(arr.unshift("end"));
console.log(arr);
4. shift()
console.log(arr.shift());
console.log(arr);
5. reverse()
console.log(arr.reverse());
6. indexOf()
console.log(arr.indexOf("1234567"));
console.log(arr);
7. lastIndexOf()
console.log(arr.lastIndexOf(null, 3));
console.log(arr);
8. includes()
console.log(arr.includes("Vasya"));
console.log(arr);
9. splice()
console.log(arr.splice(4));
console.log(arr);
10. sort()
arr = [5, 7, 34, 987, 0, 24];
arr.sort((a, b) => b - a);
console.log(arr);
11. forEach()
arr.forEach((value, index) =>
  console.log(
    `Спочатку виводиться індекс - ${index}, а потім  значення - ${value} цього масива.`
  )
);
console.log(arr);
12. findIndex();
function searchIndex(value, index) {
  if (typeof value === `string`) {
    console.log(`Індекс - ${index}, значення  - "${value}"`);
  }
  return;
}
console.log(arr.findIndex(searchIndex));
console.log(arr);




    Повертають новий масив:

1. slice()
arrNew = arr.slice(1);
console.log(arrNew);
console.log(arr);
2. concat()
arrNew = arr.concat("hello, peace", 769, null, function () {}, "1234567");
console.log(arrNew);
console.log(arr);
3. map()
arrNew = arr.map((value) => value * 3);
console.log(arrNew);
console.log(arr);
4. filter()
arrNew = arr.filter((value) => value > 1);
console.log(arrNew);
console.log(arr);
5. find()
arrNew = arr.find((value) => value > 1);
console.log(arrNew);
console.log(arr);
6. findIndex()
arrNew = arr.findIndex((value, index) => index > 1);
console.log(arrNew);
console.log(arr);
7. reduce()
arrNew = arr.reduce((srartValue, valueMass) => srartValue + valueMass);
console.log(arrNew);
console.log(arr);
7. reduceRight()
arrNew = arr.reduceRight((srartValue, valueMass) => srartValue + valueMass);
console.log(arrNew);
console.log(arr);

    3. Як можна перевірити, що та чи інша змінна є масивом? 

Можна використати Array.isArray(змінна). Поверне true або false.

    4. В яких випадках краще використовувати метод map(), а в яких forEach()?

Це різні методи: map() - використовується для копіювання масиву в новий масив, а forEach() - перебирає кожен елемент масиву.

                    Практичні завдання:

 1. Створіть масив з рядків "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.
 
 const arr = ["travel", "hello", "eat", "ski", "lift"];
let arrNew = arr.filter((value) => value.length > 3).length;
console.log(`Рядків з довжиною більше за 3 символи: ${arrNew}`);

 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча". 
 Відфільтрований масив виведіть в консоль.

 const person = [
  { name: "Іван", age: 28, sex: "чоловіча" },
  { name: "Петро", age: 27, sex: "чоловіча" },
  { name: "Аліна", age: 26, sex: "жіноча" },
  { name: "Марічка", age: 25, sex: "жіноча" },
];

let personNew = person.filter((value) => value.sex === "чоловіча");
console.log(personNew);

 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)

Технічні вимоги:

- Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
 */
