"use strict";

// ## TASK 1.
// Необхідно «оживити» меню навігації за допомогою JavaScript.
// При натисканні на елемент меню додавати до нього CSS-клас .active.
// Якщо такий клас вже існує на іншому елементі меню, необхідно зняти попередній елемент CSS-класу .active.
// Кожен елемент меню – це посилання, що веде на google.
// За допомогою JavaScript необхідно запобігти перехід до всіх посилання на цей
// зовнішній ресурс.
// Умови:
// - У реалізації обов'язково використовувати прийом делегування подій (на весь скрипт слухач має бути один).
//  <style>
//     .active {
//         background-color: yellow;
//     }
//  </style>

//    <ul id="menu">
//    <li><a href="https://google.com">PHP</a>
//    <ul>
//    <li><a href="https://google.com">Справочник</a></li>
//    <li><a href="https://google.com">Сниппеты</a></li>
//    </ul>
//    </li>
//    <li><a href="https://google.com">HTML</a>
//    <ul>
//    <li><a href="https://google.com">Информация</a></li>
//    <li><a href="https://google.com">Примеры</a></li>
//    </ul>
//    </li>
//    </ul>
const list = document.querySelector("#menu");
list.addEventListener("click", function onListClick(e) {
  e.preventDefault();

  if (e.target.tagName === "A") {
    const link = [...list.querySelectorAll("a")];
    link.forEach((el) => {
      el.classList.remove("active");
    });
    e.target.classList.add("active");
    console.log(link);
  }
});
