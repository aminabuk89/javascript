`use strict`;
/* 
                    Теоретичні питання:

    1. Опишіть своїми словами що таке Document Object Model (DOM)

Це головний документ, в якому є вузли (теги, коментарі, текстові ноди), які зв'язані між собою гілками.

    2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

В середину властивості innerHTML можна записувати і теги і текст, а на сторінці буде відображатися тільки текст. Тобто, доречно використовувати з будь-якою вкладенністю тегів і будь-якою кількістю тексту. А в середину  властивості innerText якщо ми пропишемо і тег і текст всередині нього, то на сторінці буде відображатися все, що написано в лапках "". Доречно використовувати, якщо ми хочемо або змінити текст на інший, або додати текст до тексту. Наприклад:
const feature = document.querySelector(".feature");
feature.innerHTML += "<p class = `feature__text`>Якийсь текст</p>";
feature.innerText += "<p class = `feature__text`>Якийсь текст</p>";

    3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

До елемента сторінки за допомогою JS можна звернутися кількома способами:
1. .querySelector("") - цей метод виводить перший елемент за селектором. БІЛЬШ ОПТИМАЛЬНИЙ СПОСІБ.
2. .querySelectorAll("") - цей метод виводить всі елементи за цим селектором у вигляді NodeList[]. БІЛЬШ ОПТИМАЛЬНИЙ СПОСІБ.
3. .getElementById("") - метод виводить цей елемент і всі вкладені теги в нього з текстом в середині за його id
4. .getElementsByClassName("") - цей метод виводить всі елементи за цим класом у вигляді HTMLCollection[]
5. .getElementsByTagName("") - цей метод виводить всі елементи за цим тегом у вигляді HTMLCollection[]
6.  ще звертатися можем через властивості children (буде HTMLCollection{}, також може буди firstElementChild - перший тег, firstChild - перша нода, lastElementChild - останній тег, lastChild - остання нода) та childNodes  (буде NodeList[])
7. а також можна звернутися через властивості сусідів: nextSibling - наступна нода сусід,  nextElementSibling - наступний тег сусід, previousSibling - попередній нода сусід, previousElementSibling - попередній тег сусід; і через батька: parentNoda - батьківська нода, parentElement - батьківський element.

    4. Яка різниця між nodeList та HTMLCollection?

NodeList - це список всіх вузлів (теги, коментарі, текстові ноди), майже []
HTMLCollection - це список всіх елементів або тегів, майже {}
                    
                    Практичні завдання:

    1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль. Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

const elementFeature = document.getElementsByClassName("feature");
або
const elementFeature = document.querySelectorAll(".feature");
console.log(elementFeature);
elementFeature.forEach((el) => (el.style.textAlign = "center"));

    2. Змініть текст усіх елементів h2 на "Awesome feature".

const h2 = document.querySelectorAll(".feature-title");
h2.forEach((el) => (el.textContent = "Awesome feature"));
console.log(h2);

    3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

// const elementTitle = document.querySelectorAll(".feature-title");
// elementTitle.forEach((el) => (el.textContent += "!"));
