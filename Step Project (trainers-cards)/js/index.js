"use strict";
const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];

const originalData = [...DATA];
let sortedData = [...originalData];
const trainersCards = document.querySelector(".trainers-cards__container");
const tempTrainerCard = document.querySelector("#trainer-card");
DATA.forEach((trainer) => {
  const cloneCard = document.importNode(tempTrainerCard.content, true);
  const trainerName = cloneCard.querySelector(".trainer__name");
  trainerName.textContent = `${trainer["first name"]} ${trainer["last name"]}`;
  const trainerPhoto = cloneCard.querySelector(".trainer__img");
  trainerPhoto.src = trainer.photo;
  trainersCards.append(cloneCard);
}); //

//вивели блоки фільтрів та сортування
const sidebar = document.querySelector(".sidebar");
sidebar.removeAttribute("hidden");
const sorting = document.querySelector(".sorting");
sorting.removeAttribute("hidden");
//

const tempTrainerCardFull = document.querySelector("#modal-template");
const cloneCardFull = tempTrainerCardFull.content.cloneNode(true);
const modal = cloneCardFull.querySelector(".modal");

trainersCards.addEventListener("click", function (event) {
  if (event.target.closest(".trainer__show-more")) {
    document.body.style = "overflow: hidden";
    let photoCa = event.target.parentElement
      .querySelector(".trainer__img")
      .getAttribute("src");
    console.log(photoCa);

    DATA.forEach((onClick) => {
      if (photoCa === onClick.photo) {
        console.log(onClick.photo);
        let trainerNameFull = modal.querySelector(".modal__name");
        trainerNameFull.textContent = `${onClick["first name"]} ${onClick["last name"]}`;
        let trainerPhotoFull = modal.querySelector(".modal__img");
        trainerPhotoFull.src = onClick.photo;
        let category = modal.querySelector(".modal__point--category");
        category.textContent = `Категорія: ${onClick.category}`;
        let experience = modal.querySelector(".modal__point--experience");
        experience.textContent = `Досвід: ${onClick.experience}`;
        let specialization = modal.querySelector(
          ".modal__point--specialization"
        );
        specialization.textContent = `Напрям тренера: ${onClick.specialization}`;
        let desc = modal.querySelector(".modal__text");
        desc.textContent = onClick.description;
        document.body.append(modal);
      }
    });
  }
  const btnModal = document.querySelector(".modal__close");
  btnModal.addEventListener("click", () => {
    document.body.style = "overflow: auto";
    modal.remove();
  });
});

//сортування масиву
const btnCkj = document.querySelectorAll(".sorting__btn");
const sortingBtn = document.querySelector(".sorting");
let sortingProperty;
sortingBtn.addEventListener("click", function (event) {
  const buttonsS = sortingBtn.children;
  if (event.target.classList.contains("sorting__btn")) {
    sortingProperty = event.target.dataset.sorting;
    for (let i = 0; i < buttonsS.length; i++) {
      if (buttonsS[i] === event.target) {
        event.target.classList.add("sorting__btn--active");
      } else {
        buttonsS[i].classList.remove("sorting__btn--active");
      }
    }
  }

  sortArray();
});
function sortArray() {
  // сортування за прізвищем
  if (sortingProperty === "last name") {
    sortedData.sort(function (a, b) {
      return a["last name"].localeCompare(b["last name"]);
    });
    updateTrainersList();
    console.log(sortedData);
    //сортування за досвідом
  } else if (sortingProperty === "experience") {
    sortedData.sort((num1, num2) => {
      const year1 = parseInt(num1.experience);
      const year2 = parseInt(num2.experience);
      return year2 - year1;
    });
    updateTrainersList();
  } else {
    sortedData = [...originalData];
  }
  updateTrainersList();
}
// функція виводить оновлений масив
function updateTrainersList() {
  trainersCards.innerHTML = "";
  sortedData.forEach((trainerSort, index) => {
    const trainerCard = document.createElement("div");
    trainerCard.className = "div-trainers__cards";
    trainerCard.innerHTML = `
    <li class="trainer">
      <img src="${trainerSort.photo}" class="trainer__img" alt="" width="280" height="300" />
      <p class="trainer__name">${trainerSort["first name"]} ${trainerSort["last name"]}<p>
     <button class="trainer__show-more" type="button" data-index="${index}">ПОКАЗАТИ</button>
     </li>`;
    trainersCards.append(trainerCard);
  });
}

//фільтрація масива за спеціальністю та категорією
const form = document.querySelector(".sidebar__filters");
const legend = form.querySelectorAll(".filters__fieldset");
let directionCategory = "ВСІ";
let directionSpecial;
let filterData, directionSpecialTarget;
legend.forEach((value, i) => {
  value.className += ` direction${i + 1}`;
});
const specializationSelect = form.querySelector(".direction1");
const categorySelect = form.querySelector(".direction2");
specializationSelect.addEventListener("click", (e) => {
  if (e.target.closest("label")) {
    directionSpecialTarget = e.target.textContent.trim();
    directionSpecial =
      directionSpecialTarget.charAt(0).toUpperCase() +
      directionSpecialTarget.slice(1).toLowerCase();
  }
  if (e.target.closest("label") && directionSpecial === "Всі") {
    directionSpecial = e.target.textContent.trim().toLowerCase();
  }
});
categorySelect.addEventListener("click", (e) => {
  if (e.target.closest("label")) {
    directionCategory = e.target.textContent.trim().toLowerCase();
  }
});
form.addEventListener("submit", (element) => {
  element.preventDefault();
  filterArray();
  renewAr();
  // console.log(filterData);
});
function filterArray() {
  filterData = DATA.filter((arrayData) => {
    return (
      (directionSpecial === "всі" ||
        arrayData.specialization === directionSpecial) &&
      (directionCategory === "всі" || arrayData.category === directionCategory)
    );
  });
  sortedData = filterData.slice();

  sortArray();
  renewAr();
}
function renewAr() {
  trainersCards.innerHTML = "";
  sortedData.forEach((trainerSort, index) => {
    const trainerCard = document.createElement("div");
    trainerCard.innerHTML = `
      <li class="trainer">
        <img src="${trainerSort.photo}" class="trainer__img" alt="" width="280" height="300" />
        <p class="trainer__name">${trainerSort["first name"]} ${trainerSort["last name"]}</p>
        <button class="trainer__show-more" type="button" data-index="${index}">ПОКАЗАТИ</button>
      </li>`;
    trainersCards.append(trainerCard);
  });
}
