/*                  Теоритичні питання:
  1. В чому полягає відмінність localStorage і sessionStorage?
  
Об'єкти веб-сховища дозволяють зберігати дані в браузері у вигляді ключ/значення.
localStorage - відрізняється тим, що дані залишається після перевантаження, і коли переходиш з цього сайту на інші - то також зерігається інформація; а при sessionStorage - дані при закритті/відкритті браузера не зберігаютсья на сторінці, і при переході на інші сайти - також.

  2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?

1.Перед зберіганням чутливої інформації у localStorage чи sessionStorage шифруємо її.
2.Не зберігаємо паролі відкритим текстом.
3. Не зберігаємо паролі на клієнтському боці (у localStorage чи sessionStorage).
4. Частіще оновлюємо та перевіряємо збережені дані в об'єктах веб-сховища.

  3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

Вони не збережуться.

                    Практичне завдання:

  Реалізувати можливість зміни колірної теми користувача.
Технічні вимоги:
- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.
Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.
Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

let buttonsStyle = document.createElement("button");
buttonsStyle.className = "btn__styles";
buttonsStyle.textContent = "Змінити тему";
buttonsStyle.style.cssText =
  "color: white; background-color: rgba(16, 187, 213, 1); font-size: 30px; padding: 10px; border: 2px solid rgba(16, 187, 213, 1);  position: fixed; right: 15px; top: 600px; z-index: 1;";

document.querySelector(".article-main").prepend(buttonsStyle);
buttonsStyle.addEventListener("click", function toggleThemeClick() {
  const bodyClass = document.body.className;
  const main = document.querySelector("main");
  const bodyDark = bodyClass === "dark-theme";
  const mainDark = main.className === "dark-theme";
  if ((!bodyDark, !mainDark)) {
    document.body.className = "dark-theme";
    main.className = "dark-theme";
  } else {
    document.body.className = "light-theme";
    main.className = "light-theme";
  }
  localStorage.setItem("theme", document.body.className);
  localStorage.setItem("theme", main.className);
});

const savedTheme1 = localStorage.getItem("theme1");
const savedTheme2 = localStorage.getItem("theme2");
if ((savedTheme1, savedTheme2)) {
  document.body.className = savedTheme1;
  main.className = savedTheme2;
}
