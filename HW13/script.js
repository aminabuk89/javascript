/*                  Теоретичні питання:

    1. Яке призначення методу event.preventDefault() у JavaScript?

event.preventDefault() - видаляє властивості браузера за замовченням (наприклад, при submit це перевантаження браузера)

    2. В чому сенс прийому делегування подій?

Сенс в тому, що ми через батьківський елемент довіряємо обробити події на БАГАТЬОХ дочірніх. Оптимізується код.

    3. Які ви знаєте основні події документу та вікна браузера? 

Основні події документу:
1. DOMContentLoaded,
2. load,
3. beforeunload/unload.

Основні події  вікна браузера:
1.DOMContentLoaded,
2.load,
3.beforeunload,
4.unload.
                    Практичне завдання:

    Реалізувати перемикання вкладок (таби) на чистому Javascript.
Технічні вимоги:
- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).

const buttons = document.querySelector(".tabs");
const textWrap = document.querySelector(".tabs-content");
buttons.addEventListener("click", function onTabClick(event) {
  const itemTabs = buttons.children;
  const textBlocks = textWrap.children;

  if (!event.target.classList.contains("active")) {
    for (let i = 0; i < itemTabs.length; i++) {
      if (itemTabs[i] === event.target) {
        event.target.classList.add("active");
        textBlocks[i].classList.add("active");
      } else {
        itemTabs[i].classList.remove("active");
        textBlocks[i].classList.remove("active");
      }
    }
  }
})
*/
