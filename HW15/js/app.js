/*                  Теоритичні питання:

    1. В чому відмінність між setInterval та setTimeout?

setTimeout() - запускаэ функцію один раз через певний час, а setInterval() -  запускаэ функцію багато разів через певний час.

    2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?

Ні, може бути невелика похибка(+ -)

    3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

для setTimeout() - використовуємо clearTimeout();
для setInterval() - використовуємо clearInterval();

                    Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const root = document.querySelector("#root");
root.style.cssText = "font-size: 40px; padding: 40px";
const btn = document.querySelector("#btn");
btn.style.cssText =
  "font-size: 30px; padding: 20px; background-color: blue; border: 4px solid black";
btn.addEventListener("click", function onButtonClick() {
  setTimeout(() => {
    root.textContent = "Операція виконана успішно))))";
  }, 3000);
});

                    Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/
// const btn = document.querySelector("#btn");
// btn.remove();
// const root = document.querySelector("#root");
// root.style.cssText = "font-size: 40px; padding: 40px";
// root.textContent = "";

// let counter = 10;
// function outputsNumbers() {
//   if (counter > 0) {
//     root.textContent = counter; 
//       counter--; 
//   } else {
//       clearInterval(timerID); 
//       root.textContent = "Зворотній відлік завершено"; 
//   }
// }
// const timerID = setInterval(outputsNumbers, 1000);
