"use strict";

/* 
                    Теоретичні питання:

    1. Що таке цикл в програмуванні?

Цикл - це інструмент, який допомагає виконати код декілька разів.

    2. Які види циклів є в JavaScript і які їх ключові слова?

Види циклів:
1. For (цикл-лічильник) -  призначений для перебирання заздалегідь відомих значеня. 
for (ініціалізація(let i = 0); умова(i <= 4); ітерація(++і або i++)){
    код;
}
2. While - спочатку перевіряє умову, а потім дію.
while (умова){
    код;
}
3. Do while - спочатку do виконає дію, а потім while перевіряє умову.
do{
    дія;
    break;
}
while (умова){
    код;
}

    3. Чим відрізняється цикл do while від while?

Цикл while спочатку перевіряє умову, а потім дію (називають циклом з перевіркою перед кожною ітерацією), а цикл do while - спочатку виконає дію, а потім перевіряє умову (називають циклом після перевірки).


                    Практичні завдання:

    1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. Виведіть на екран всі числа від меншого до більшого за допомогою циклу for. 

let number1, number2;

while (true) {
  number1 = prompt("Введіть перше число:");

  if (number1 !== null && number1 !== "" && !isNaN(number1)) {
    let minNumber = parseInt(Math.min(number1, number2));
    let maxNumber = parseInt(Math.max(number1, number2));
    for (let i = minNumber; i <= maxNumber; i++) {
      console.log(i);
    }
    break;
  } else {
    alert("Введіть, будь ласка, саме число!");
  }
}

while (true) {
  number2 = prompt("Введіть друге число:");

  if (number2 !== null && number2 !== "" && !isNaN(number2)) {
    let minNumber = parseInt(Math.min(number1, number2));
    let maxNumber = parseInt(Math.max(number1, number2));
    console.log(`Числа від ${minNumber} до ${maxNumber}:`);
    for (let i = minNumber; i <= maxNumber; i++) {
      console.log(i);
    }
    break;
  } else {
    alert("Введіть, будь ласка, саме число!");
  }
}

    2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. Якщо введене значення не є парним числом, 
    то запитуйте число доки користувач не введе правильне значення.
*/

// let inputNumber;
// while (true) {
//   inputNumber = prompt("Введіть, будь ласка, парне число:");
//   if (inputNumber % 2 === 0) {
//     console.log(`Парне число ${inputNumber}`);
//     break;
//   } else {
//     alert("Введіть, будь ласка, саме парне число!");
//   }
// }
