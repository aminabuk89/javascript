/* 
                        Теоретичні питання:

    1. Що таке події в JavaScript і для чого вони використовуються?

Подія - це дії, які може робити користувач на сайті (виділення, натискання, копіювання, рух миші, наведення). Події використовуються для того, щоб взаємодіяти з сайтом (наприклад, при натисканні на кнопку вивести параграф, або при натисканні на параграф пофарбувати задній фон в червоний)

    2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

Події миші:
1. натискання: click, dblcick, mousedown, mouseup, contextmenu
2. переміщення курсора: mouseenter, mouseleave, mouseover, mouseout
3. mousemove, select, wheel

    3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Подія "contextmenu" - це клікання правою кнопкою миші на сайті по контекстному меню. Використовується для того, щоб встановити якусь реакцію на контексне меню (скрити, показати).

                        Практичні завдання:

    1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

const btn = document.getElementById("btn-click");
const section = document.querySelector("#content");
btn.addEventListener("click", function onBtnClick() {
  const p = document.createElement("p");
  p.textContent = "New Paragraph";
  section.append(p);
});

    2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

// const btn = document.createElement("button");
// const section = document.querySelector("#content");
// btn.id = "btn-input-create";
// section.append(btn);
// btn.addEventListener("click", function onButtonClick() {
//   const input = document.createElement("input");
//   input.type = "checkbox";
//   input.placeholder = "Ви погоджуєтесь з умовами сайту?";
//   input.name = "text";
//   btn.after(input);
// });
