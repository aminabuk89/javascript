/* 
                    Теоретичні питання:

    1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

За допомогою властивості event.code() та event.key() можна визначити, яку саме клавішу клавіатури натиснув користувач.

    2. Яка різниця між event.code() та event.key()?

event.key() - краще використовувати, коли хочемо перевірити, яку саме фізичну клавішу натиснув користувач (ctrl, caps lock, tab, alt).
event.code() - краще використовувати, коли хочемо перевірити, яку саме букву натиснув користувач (a, M, r, P).

    3. Які три події клавіатури існує та яка між ними відмінність?

Події клавіатури:
1. keydown - натискання клавіші;
2. keypress - ця подія між цими двома діями;
3. keyup - відпустили клавішу.

                    Практичне завдання:

    Реалізувати функцію підсвічування клавіш.
Технічні вимоги:
- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/

const buttons = document.querySelectorAll(".btn");
console.log(buttons);
window.addEventListener("keydown", function onButtonKeydown(e) {
  for (let index = 0; index < buttons.length; index++) {
    if (e.code === "Enter") {
      index.classList.add("btn__accent");
      buttons.classList.remove("btn__accent");
    }
    if (e.code === "KeyS") {
      index.classList.add("btn__accent");
      buttons.classList.remove("btn__accent");
    }

    //     // el2.classList.add("btn__accent");
    //     console.log(el2);
    //   }
  }
});

// buttons.forEach(function (el2) {
//   if (el2 !== el1) {
//     el2.classList.remove("btn__accent");
//   }
// });

// const list = document.querySelector("#menu");
// list.addEventListener("click", function onListClick(e) {
//   e.preventDefault();

//   if (e.target.tagName === "A") {
//     const link = [...list.querySelectorAll("a")];
//     link.forEach((el) => {
//       el.classList.remove("active");
//     });
//     e.target.classList.add("active");
//     console.log(link);
//   }
// });
